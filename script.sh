#!/bin/bash

if (($# != 3)); then
    echo "Usage: ./script.sh <input_file> <clean_template> <output_file>"
    exit 1
fi

cp $2 $3 

img="img.png"

file=$1

positions=(0 0 0 0 0 0)
extensions=(".jpg" ".png" ".gif" ".jpeg" ".tiff")

instance="https://nitter.actionsack.com/"

for line in $(cat $file)
do
    username=$(echo $line | cut -d"-" -f1)
    pos=$(echo $line | cut -d"-" -f2)
    ypos=0
    ((xpos=140+${positions[$pos]}*124))

    ((positions[$pos]=${positions[$pos]}+1))

    imagelink=""
    i=0

    echo "Putting $username in position $pos..."
    
    while [ -z "$imagelink" ]; do
	imagelink=$(curl -s $instance$username | grep "400x400${extension[$i]}" | head -n 2 | tail -n 1 | cut -f3 -d= | cut -f1 -d" " | cut -f2 -d"\"")
	((i=$i+1))
    done

    wget -q -O $img $imagelink

    case $pos in
	"0")
	    ypos=80
	    ;;
	"1")
	    ypos=210
	    ;;
	"2")
	    ypos=338
	    ;;
	"3")
	    ypos=460
	    ;;
	"4")
	    ypos=576
	    ;;
	"5")
	    ypos=690
	    ;;
    esac
    
    magick mogrify -resize 102x102 $img 

    magick composite -compose atop -geometry +$xpos+$ypos img.png $3 $3

done

rm img.png
